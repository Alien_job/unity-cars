﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerMove : MonoBehaviour {

    public int MoveSpeed;
    public int RotationSpeed;

    public float MaxSpeed;
    public float MaxRotationSpeed;
    public float MaxAcceleration;

    public float Speed = 0;
    public GameObject speedSlider;
    public GameObject accelerationSlider;

    // Use this for initialization
    void Start () {
        //speedSlider = GameObject.FindGameObjectWithTag("speedSlider");
        //accelerationSlider = GameObject.FindGameObjectWithTag("accelerationSlider");
    }

    float limit (float volume, float limit)
    {
        if (volume > 0)
            return Mathf.Min(volume, limit);
        else
            return Mathf.Max(volume, -limit);
 
    }
    // Update is called once per frame
    void Update () {

        float acceleration = (Input.mousePosition.y - (Camera.main.pixelHeight / 3));
        acceleration = limit(acceleration, MaxAcceleration);

        if ((acceleration < 0)||(Speed < 5)) acceleration *= 5;
        acceleration *= 3;

        Speed = Speed + acceleration / 100000f;
        Speed = limit(Speed, MaxSpeed);

        float dZRotation = -(Input.mousePosition.x - (Camera.main.pixelWidth / 2));
        dZRotation = limit(dZRotation, MaxRotationSpeed);

        transform.Rotate(0,0, RotationSpeed * dZRotation * Speed/20000);

        float posX = (Mathf.Sin((transform.localEulerAngles.z) * Mathf.Deg2Rad) );
        float posY = (Mathf.Cos((transform.localEulerAngles.z) * Mathf.Deg2Rad));
        Vector3 move = new Vector3(-posX * Speed, posY * Speed) ;

        transform.position += (move * Time.deltaTime  * MoveSpeed / 10);

        speedSlider.GetComponent<Slider>().value = Mathf.Abs(Speed);
        accelerationSlider.GetComponent<Slider>().value = Mathf.Abs(acceleration);


        //Debug.Log("speed" + Speed + "acceleration" + acceleration + "move" + move
         //   + "localEulerAngles" + transform.localEulerAngles + "posX" + posX + "posY" + posY
          //  );
        //Debug.Log("transform.localPosition" + transform.position);
        //Debug.Log("Input.mousePosition" + Input.mousePosition);
        //Debug.Log("Distance" + Vector3.Distance(transform.localPosition, Input.mousePosition));
        //Debug.Log("screenDistance" + Vector3.Distance(screenPoint, Input.mousePosition));

    }
}

