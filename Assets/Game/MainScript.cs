﻿using UnityEngine;
using System.Collections;

public class MainScript : MonoBehaviour {

    public GameObject[] Race;

    public GameObject ButtomTop;
    public GameObject ButtomLeft;
    public GameObject ButtomRight;
    public GameObject LeftRight;
    public GameObject LeftTop;
    public GameObject RightLeft;
    public GameObject RightTop;

    public void nextRoad()
    {
        try
        {
            GameObject dead = Race[0].gameObject;
            for (int i = 1; i < 50; i++)
            {
                Race[i - 1] = Race[i];
            }
            Transform point = Race[48].transform.GetChild(0);
            Race[49] = Instantiate(next(Race[48]), point.position, point.rotation) as GameObject;

            Destroy(dead, 5);
        }
        catch
        {
            Debug.Log("nextRoad()");
        }
    }

    // Use this for initialization
    void Start () {

        GameObject current = ButtomTop;
        for (int i = 2; i <50; i++)
        {
            Transform point = Race[i - 1].transform.GetChild(0);
            GameObject go = Instantiate(next(current), point.position, point.rotation) as GameObject;
            onCollision currentOnCollision = go.GetComponent<onCollision>() as onCollision;
            currentOnCollision.Index = i;
            current = go;
            Race[i] = go;
        }
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
            Debug.Log("SPACE");

	}



    GameObject next(GameObject current)
    {
        int j = 0;
        if ((current.name.Contains("BottomTop")) || (current.name.Contains("LeftTop")) || (current.name.Contains("RightTop"))){
            j = Mathf.RoundToInt(Random.value * 2.9f - 0.49f);
            Debug.Log(current.name + " " + j);
            if (j == 0) return ButtomTop;
            if (j == 1) return ButtomLeft;
            if (j == 2) return ButtomRight;
        }
        if ((current.name.Contains("BottomLeft")) || (current.name.Contains("RightLeft"))){
            j = Mathf.RoundToInt(Random.value * 1.9f - 0.49f);
            Debug.Log(current.name + " " + j);
            if (j == 0) return RightTop;
            if (j == 1) return RightLeft;
        }
        if ((current.name.Contains("BottomRight")) || (current.name.Contains("LeftRight"))){
            j = Mathf.RoundToInt(Random.value * 1.9f - 0.49f);
            Debug.Log(current.name + " " + j);
            if (j == 0) return LeftTop;
            if (j == 1) return LeftRight;
            }
        return ButtomTop;
    }
}

