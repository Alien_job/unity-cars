﻿using UnityEngine;
using System.Collections;

public class onCollision : MonoBehaviour {

    public int Index;

    void OnCollisionEnter2D(Collision2D other)
    {
        //Debug.Log("OnCollisionEnter2D");
        if(other.gameObject.GetComponent<PlayerMove>())
            other.gameObject.GetComponent<PlayerMove>().Speed *= (8f/10f);
    }

    void OnTriggerExit2D(Collider2D other)
    {
        Debug.Log("OnTriggerEnter2D");
        GameObject mainObject = GameObject.Find("Main");
        mainObject.GetComponent<MainScript>().nextRoad();
        
        
    }
}
